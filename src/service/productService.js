import { api } from "../utils/api";

export const FETCH_ALL_PRODUCTS = async () => {
  let response = await api.get("/products");
  return response.data;
};

// post product
export const POST_PRODUCT = async (product) => {
  let resposne = await api.post("/products", product);
  return resposne.data;
};
