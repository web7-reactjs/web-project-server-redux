import axios from "axios";
import { api } from "../utils/api";

export const UPLOAD_FILE = async (data )=>{
    
    const response  = await api.post("/files/upload",data,{})
    return response.data 

}