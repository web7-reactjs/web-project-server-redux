import { api } from "../utils/api";

// service for the users
export const FETCH_ALL_USERS = async () => {
  let response = await api.get("users");
  return response.data 
};

export const CREATE_USER = async(user)=>{
    let response = await api.post("users",user)
    return response
}

export const FIND_USER_BY_ID = async(id)=>{
    let response = await api.get(`/users/${id}`)
    return response.data
}
  

export const UPDATE_USER = async(id,user)=>{
  let response = await  api.put(`/users/${id}`,user)
  return response.data
}