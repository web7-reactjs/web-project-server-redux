import { api } from "../utils/api";

 export const GET_ALL_CATEGORIES = async()=>{
    let response = await api.get("/categories")
    return response.data
 }