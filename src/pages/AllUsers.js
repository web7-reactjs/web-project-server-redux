import axios from "axios";
import React from "react";
import { useEffect, useState } from "react";
import MyCard from "../components/MyCard";
import { FETCH_ALL_USERS } from "../service/userService";
import { Bars } from "react-loader-spinner";

const AllUsers = () => {
  const [allUsers, setAllUsers] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true)
    FETCH_ALL_USERS()
      .then((res) => {
        setAllUsers(res);
        setLoading(false);
      })
      .catch((err) => console.log("Error : " + err));
  }, []);

  let sortedUsers = allUsers.sort((user1, user2) =>
    user1.id > user2.id ? -1 : user1.id < user2.id ? 1 : 0
  );

  return (
    <>
      <div className="container">
        <div className="row ">
          {loading ? (
            <div className="d-flex justify-content-center">
              <Bars
                height="80"
                width="80"
                radius="9"
                color="orange"
                ariaLabel="loading"
                wrapperStyle
                wrapperClass
              />
            </div>
          ) : (
            <>
              {sortedUsers.map((user, index) => (
                <div
                  key={index}
                  className="col-3 d-flex mt-3 justify-content-center"
                >
                  <MyCard user={user} />
                </div>
              ))}
            </>
          )}
        </div>
      </div>
    </>
  );
};

export default AllUsers;
