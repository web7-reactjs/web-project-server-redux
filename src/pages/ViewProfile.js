import React, { useState } from "react";
import { useParams } from "react-router-dom";
import { useEffect } from "react";
import { FIND_USER_BY_ID, UPDATE_USER } from "../service/userService";
import MyCard from "../components/MyCard";
import { toast } from "react-toastify";
import { ToastContainer } from "react-toastify";
import { NavLink } from "react-router-dom";
import { UPLOAD_FILE } from "../service/fileService";
import { Bars } from "react-loader-spinner";
const ViewProfile = () => {
  const { id } = useParams();
  const [user, setUser] = useState({});
  const [buttonText, setButtonText] = useState("");
  const [styleButton, setStyleButton] = useState("");
  const [inputStatus, setInputStatus] = useState(true);
  // states for the user update
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [role, setRole] = useState("");
  const [loading, setLoading] = useState(false);
  const [imageUrl, setImageUrl] = useState("");
  const [selectedFile, setSelectedFile] = useState("");

  useEffect(() => {
    FIND_USER_BY_ID(id)
      .then((response) => {
        setUser(response);
        setName(response.name);
        setPassword(response.password);
        setEmail(response.email);
        setRole(response.role);
        // setting value to the input field
      })
      .catch((err) => console.log("Error getting the user : ", err));
  }, []);

  console.log("User is : ", user);
  if (!buttonText) setButtonText("Update");
  const updateButton = () => {
    if (buttonText == "Submit") {
      setButtonText("Update");
      setInputStatus(true);

      setLoading(true);

      const data = new FormData();
      data.append("file", selectedFile);

      // if they select the file
      // we upload the file
      // we only update the information
      if (selectedFile) {
        UPLOAD_FILE(data)
          .then((response) => {
            updatedUser.avatar = response.location;

            UPDATE_USER(id, updatedUser)
              .then((response) => {
                if (response) {
                  toast("Successfully Update User");
                  setLoading(false);
                }
              })
              .catch((err) => {
                toast("Failed to updated user ");
              });
          })
          .catch((err) => toast("Error : " + err));
      } else {
        UPDATE_USER(id, updatedUser)
          .then((response) => {
            if (response) {
              toast("Successfully Update User");
              setLoading(false);
            }
          })
          .catch((err) => {
            toast("Failed to updated user ");
          });
      }

      console.log("here is the value of the updatedUser : ", updatedUser);
    } else {
      setButtonText("Submit");
      setInputStatus(false);
      // allow to update the content
    }
  };

  const nameChange = (e) => {
    setName(e.target.value);
  };
  const passChange = (e) => {
    setPassword(e.target.value);
  };

  const emailChange = (e) => {
    setEmail(e.target.value);
  };
  const roleChange = (e) => {
    setRole(e.target.value);
  };

  let updatedUser = {
    name: name,
    password: password,
    role: role,
    email: email,
    avatar:
      "https://i.pinimg.com/564x/9c/e3/d5/9ce3d5e48bb7f511be3a6fe6e74cf4ff.jpg",
  };

  console.log(" Updated User : ", updatedUser);

  const onChangeImage = (e) => {
    if (e.target.files && e.target.files[0]) {
      let reader = new FileReader();
      reader.onload = (e) => {
        setImageUrl(e.target.result);
      };
      reader.readAsDataURL(e.target.files[0]);
      // getting value for the profile update
      setSelectedFile(e.target.files[0]);
    }
  };
  return (
    <div className="container d-flex bg-light gap-5 mt-5 py-5 rounded-5 justify-content-center ">
      <div className="profile w-25 ">
        <img
          className="object-fit-contain w-100"
          src={
            imageUrl
              ? imageUrl
              : user.avatar
              ? user.avatar
              : "https://www.pngkey.com/png/full/73-730477_first-name-profile-image-placeholder-png.png"
          }
          alt="user profile image"
        />
        <input
          className="form-control"
          type="file"
          disabled={inputStatus}
          onChange={onChangeImage}
        />
      </div>
      <div className=" ">
        <div>
          <h1>Profile Information</h1>
          <div className="d-flex mt-5 justify-content-center align-items-center gap-3">
            <label htmlFor=""> Name </label>
            <input
              type="text"
              value={name}
              onChange={nameChange}
              disabled={inputStatus}
              className="form-control w-50"
            />

            <label htmlFor="roleSelector"> Role</label>

            <select
              className="form-control"
              name="roleSelector"
              id="roleSelector"
              value={role}
              onChange={roleChange}
              disabled={inputStatus}
            >
              <option value="admin"> Admin</option>
              <option value="customer">Customer</option>
            </select>
          </div>
          <div className="d-flex justify-content-center align-items-center mt-3 gap-3">
            <label htmlFor=""> Email</label>
            <input
              type="text"
              onChange={emailChange}
              className="form-control w-100"
              disabled={inputStatus}
              value={email}
            />
          </div>
          <div className="d-flex  align-items-center mt-3 gap-3">
            <label htmlFor=""> Password</label>
            <input
              type="text"
              className="form-control  "
              disabled={inputStatus}
              onChange={passChange}
              value={password}
            />
          </div>

          <div className="d-flex mt-4 justify-content-between ">
            <button
              className={
                buttonText == "Update" ? "btn btn-primary" : "btn btn-success"
              }
              onClick={updateButton}
            >
              {buttonText}
            </button>
            <NavLink className="nav-link" to="/users">
              <button className="btn btn-danger"> Cancel </button>
            </NavLink>
          </div>
          <ToastContainer />
        </div>
        <div className="loader d-flex justify-content-center">
          {loading ? (
            <Bars
              height="50"
              width="50"
              radius="9"
              color="orange"
              ariaLabel="loading"
              wrapperStyle
              wrapperClass
            />
          ) : (
            ""
          )}
        </div>
      </div>
    </div>
  );
};

export default ViewProfile;
