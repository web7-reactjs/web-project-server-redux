import React from "react";
import { useState, useEffect } from "react";
import { GET_ALL_CATEGORIES } from "../service/categoryService";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { clear } from "@testing-library/user-event/dist/clear";
import { UPLOAD_FILE } from "../service/fileService";
import { POST_PRODUCT } from "../service/productService";
import { Bars } from "react-loader-spinner";
const InsertProduct = () => {
  const [title, setTitle] = useState("");
  const [price, setPrice] = useState("");
  const [categoryId, setCategoryId] = useState("");
  const [productImage, setProductImage] = useState("");
  const [description, setDescription] = useState("");
  const [category, setCategory] = useState([]);
  const [imageUrl, setImageUrl] = useState("");
  const [selectedFile, setSelectedFile] = useState(null);
  const [loading, setLoading] = useState(false);
  let product = {
    title,
    price,
    description,
    categoryId: Number(categoryId),
    images: [],
  };
  function clearInputFields() {
    setTitle("");
    setPrice("");
    setCategoryId(1);
    setProductImage("");
    setDescription("");
    setImageUrl("");
  }
  const createProduct = (e) => {
    setLoading(true);
    e.preventDefault();

    if (selectedFile) {
      const data = new FormData();
      data.append("file", selectedFile);
      console.log(" >>> Value of the selected file : ", selectedFile);
      UPLOAD_FILE(data)
        .then((response) => {
          product.images.push(response.location);

          // now we are uploading the post
          POST_PRODUCT(product).then((response) => {
            if (response) {
              setLoading(false);
              clearInputFields();
              toast("Create The Product Sucessfully !! ");
              // setImageUrl(response.images[0])  
              // clear the button
            } else {
              toast("Something went wrong!!!");
            }
          });
        })
        .catch((err) => console.log("Error posting images ... ! ", err));
    } else {
      // case the api required the we need to put image at least one element
      product.images.push(
      "https://developers.elementor.com/docs/assets/img/elementor-placeholder-image.png"
      );
         console.log("product that about to insert is : ", product)
      POST_PRODUCT(product).then((response) => {
        if (response) {
          setLoading(false);
          clearInputFields();
          toast("Create The Product Sucessfully !! ");

          // clear the button
        } else {
          toast("Something went wrong!!!");
        }
      }).catch((err)=>{
        console.log("Error inserting the product : ", err)
      })
    }
  };

  const clearButton = (e) => {
    e.preventDefault();
    clearInputFields();
    setSelectedFile(null);
    // clear
    toast("Clearing Done!!! ");
  };

  useEffect(() => {
    GET_ALL_CATEGORIES()
      .then((response) => setCategory(response))
      .catch((err) => console.log("Error Fetching the category : ", err));
  }, []);

  console.log("here is the value of the product : ", product);

  let canSave = title && categoryId && description && price ? false : true;
  const onImageChange = (e) => {
    if (e.target.files && e.target.files[0]) {
      let reader = new FileReader();
      reader.onload = (e) => {
        setImageUrl(e.target.result);
      };
      reader.readAsDataURL(e.target.files[0]);
      // setting the file to useState
      setSelectedFile(e.target.files[0]);
      //
    }
  };

  if (!categoryId) {
    setCategoryId(1);
  }
  return (
    <>
      <div className="container d-flex justify-content-center bg-light mt-5 pt-5 pb-5 rounded-4  ">
        <div className="row g-5 ">
          <div className="productimage  col ">
            <img
              width="500px"
              className=" object-fit-contain"
              src={
                imageUrl
                  ? imageUrl
                  : "https://developers.elementor.com/docs/assets/img/elementor-placeholder-image.png"
              }
              alt=""
            />
            <input
              onChange={onImageChange}
              type="file"
              className="form-control"
            />
          </div>

          <div className="productInformation col  ">
            <h1> Product Information</h1>
            <form>
              <label htmlFor="inputName"> Product Name </label>
              <input
                type="text"
                value={title}
                onChange={(e) => setTitle(e.target.value)}
                className="form-control"
              />

              <div className="container    px-0 d-flex justify-content-between">
                <div className="px-0">
                  <label htmlFor="inputPrice"> Product Price </label>
                  <input
                    type="number"
                    value={price}
                    onChange={(e) => setPrice(e.target.value)}
                    className="form-control"
                  />
                </div>

                <div className="px-0">
                  <label htmlFor=""> Choose Category </label>
                  <select
                    value={categoryId}
                    onChange={(e) => setCategoryId(e.target.value)}
                    className="form-control"
                    name=""
                    id=""
                  >
                    {category.map((cat) => (
                      <option key={cat.id} value={cat.id}>
                        {cat.name}
                      </option>
                    ))}
                  </select>
                </div>
              </div>

              <label htmlFor="inputDescription">Product Description </label>
              <textarea
                type="text"
                value={description}
                onChange={(e) => setDescription(e.target.value)}
                className="form-control"
              />

              <div className="container mt-4 d-flex justify-content-between">
                {/* */}
                <button
                  disabled={canSave}
                  onClick={createProduct}
                  className="btn btn-warning "
                >
                  {" "}
                  Create Product{" "}
                </button>
                <div>
                  <button onClick={clearButton} className="btn btn-danger">
                    {" "}
                    Clear{" "}
                  </button>
                  <ToastContainer />
                </div>
              </div>
            </form>
            <div className="row">
              {loading ? (
                <div className="container  d-flex justify-content-center">
                  <Bars
                    height="80"
                    width="80"
                    radius="9"
                    color="orange"
                    ariaLabel="loading"
                    wrapperStyle
                    wrapperClass
                  />
                </div>
              ) : (
                <p> </p>
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default InsertProduct;
