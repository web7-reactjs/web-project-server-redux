import React from "react";
import { useState } from "react";
import { CREATE_USER } from "../service/userService";
import { toast, ToastContainer } from "react-toastify";
const InsertUser = () => {
    const [isUserChanged, setIsUserChanged] = useState(false);
    const [allUsers, setAllUsers] = useState([]);
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [avatar, setAvatar] = useState("");
 
  
    let user = {
      name: name,
      email: email,
      password: password,
      avatar: avatar,
    };

    const createButton = (e) => {
        e.preventDefault();
        CREATE_USER(user)
          .then((res) => {
            if (res?.data) {
              setEmail("");
              setName("");
              setAvatar("");
              setPassword("");
             
              toast("Successfully created user...!")
              // change the state to fetch the users again
              setIsUserChanged(!isUserChanged);
            } else {
               toast("Something went wrong..!")
            }
          })
          .catch((err) => console.log("Error Create new User  : ", err));
      };
      let canSave =  (name && password && email && avatar)? true : false; 
 
  return (
    <>
      <div className="container ">
        <div className="d-flex mt-5 rounded-5  bg-light  justify-content-center align-items-center gap-4">
          <div className="image w-50">
            <img
              className="object-fit-contain  w-100" alt=""
              src="https://cdn3d.iconscout.com/3d/premium/thumb/man-holding-sign-up-form-2937684-2426382.png"
            />
          </div>
          <div className="form-side  ">
              <div className="   mt-4 rounded-5 pt-5 px-3 pb-2">
                <h1> Create New Users </h1>

                <div className="my-5  ">
                  <form action="">
                    <label htmlFor="inputName"> Username </label>
                    <input
                      value={name}
                      onChange={(e) => setName(e.target.value)}
                      id="inputName"
                      type="text"
                      name="inputName"
                      placeholder="Enter username "
                      className="form-control"
                    />
                    <label htmlFor="inputEmail"> Email</label>
                    <input
                      value={email}
                      onChange={(e) => setEmail(e.target.value)}
                      id="inputEmail"
                      name="inputEmail"
                      type="text"
                      placeholder="Enter email "
                      className="form-control"
                    />
                    <label htmlFor="inputPassword"> Password </label>
                    <input
                      value={password}
                      onChange={(e) => setPassword(e.target.value)}
                      type="password"
                      id="inputPassword"
                      name="inputPassword"
                      placeholder="Enter password "
                      className="form-control"
                    />
                    <label htmlFor="inputAvatar"> Avatar </label>
                    <input
                      value={avatar}
                      onChange={(e) => setAvatar(e.target.value)}
                      type="text"
                      id="inputAvatar"
                      name="inputAvatar"
                      placeholder="Enter Avatar Links "
                      className="form-control"
                    />

                    <button
                      disabled={!canSave}
                      className="
                btn
                 btn-warning 
                 my-2"
                      onClick={createButton}
                    >
                      Create Users
                    </button>

                    <ToastContainer/>
                  </form>
                </div>
              </div>
             
             
          </div>
        </div>
      </div>
    </>
  );
};

export default InsertUser;
