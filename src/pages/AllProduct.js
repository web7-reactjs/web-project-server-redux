import React from "react";
import { useEffect, useState } from "react";
import ProductCard from "../components/ProductCard";
import { GET_ALL_CATEGORIES } from "../service/categoryService";
import { FETCH_ALL_PRODUCTS } from "../service/productService";
import { Bars } from "react-loader-spinner";
const AllProduct = () => {
  // useState
  const [products, setProducts] = useState([]);
  const [category, setCategory] = useState([]);
  const [filterValue, setFilterValue] = useState("");
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true )
    FETCH_ALL_PRODUCTS()
      .then((response) =>{ 
        
        setLoading(false)
        setProducts(response); })
      .catch((err) => console.log("error : ", err));

    // fetch for all the category as well
  }, []);
  useEffect(() => {
    GET_ALL_CATEGORIES()
      .then((response) => setCategory(response))
      .catch((err) => console.log("Error fetching call category : ", err));
    // fetch for all the category as well
  }, []);
  // writing the code to get all the products
  let filteredValue = filterValue ? true : false;

  let filteredProduct = products.filter((p) => p.category.name == filterValue);

   
  return (
    <>
      <div className="container">
        <div className="container  align-items-center justify-content-between d-flex flex-row-reverse px-5 pb-2">
          <div className="inputForm w-25">
            <label htmlFor="searchOptions"> Choose Category : </label>
            <select
              className="form-control"
              name="searchOptions"
              id="searchOptions"
              value={filterValue}
              onChange={(e) => {
                setFilterValue(e.target.value);
              }}
            >
              <option value=""> All</option>
              {category.map((cate,index) => (
                <option key={index} value={cate.name}>{cate.name}</option>
              ))}
            </select>
          </div>
          <div className="">  
          
                <h5>Total : <span className="text-warning h4">{(filteredValue ? filteredProduct.length : products.length)}</span>  records</h5>
            </div>        

        </div>

        {loading ? (
          <div className="container  d-flex justify-content-center">
                <Bars
                  height="80"
                  width="80"
                  radius = "9"
                  color ="orange"
                  ariaLabel ="loading"
                  wrapperStyle
                  wrapperClass
                />
          </div>
        ) : (
          <div className="row gx-5 gy-5  ">
            {
            // if there is records of (filteredProduct or products )
             
            (filteredValue ? filteredProduct.sort((p1,p2)=> (p1.id>p2.id)? -1  : (p1.id < p1.id)? 1 : 0) : products.sort((p1,p2)=> (p1.id>p2.id)? -1  : (p1.id < p1.id)? 1 : 0)).map(
              (product, index) => (
                <div
                  key={product.id}
                  className="col-4 d-flex justify-content-center"
                >
                  <ProductCard key={product.id+index} product={product} /> 
                </div>
              )
            )
            
            
            }
          </div>
        )}
      </div>
    </>
  );
};

export default AllProduct;
