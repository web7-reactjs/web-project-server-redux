import { Dropdown } from 'bootstrap';
import React from 'react'
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import { NavLink } from 'react-router-dom';
const MyNavBar = () => {
  return (
    <>
    <Navbar bg="light" expand="lg">
      <Container>
        <Navbar.Brand href="/">React-Bootstrap</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <NavDropdown title="Users">
              <NavLink to="/newUser" className="nav-link"> New User</NavLink>
              <NavLink to="/users" className="nav-link"> Users</NavLink>
            </NavDropdown>
             <NavDropdown title="Product" >
                <NavLink className="nav-link" to="/newproduct">New Products </NavLink>
                <NavLink className="nav-link" to="/products"> Products </NavLink>
             </NavDropdown>
            <NavLink className="nav-link" to ="/others"> Others </NavLink>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
    </>
  )
}

export default MyNavBar