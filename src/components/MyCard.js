import React from "react";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import { Link   } from "react-router-dom";
const MyCard = ({ user }) => {
 
  return (
    <>
      <Card style={{ width: "18rem" }}>
        <Card.Img variant="top"
         src={user.avatar} 
         onError={({currentTarget})=>{
          currentTarget.onerror = null; // prevent looping
          currentTarget.src= "https://st4.depositphotos.com/4329009/19956/v/600/depositphotos_199564354-stock-illustration-creative-vector-illustration-default-avatar.jpg"
         }}
         />
        <Card.Body>
          <Card.Title>
            {user.name} <span className="text-warning"> {user.role}</span>{" "}
          </Card.Title>
          <Card.Text>
            <p>{user.email}</p>
            Some quick example text to build on the card title and make up the
            bulk of the card's content.
          </Card.Text>
          <div className="d-flex justify-content-between">
            <Link to={`/users/${user.id}`}>
              <Button variant="primary">View</Button>
            </Link>

            <Button variant="warning">Update</Button>
             
          </div>
        </Card.Body>
      </Card>
    </>
  );
};

export default MyCard;
