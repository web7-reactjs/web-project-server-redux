import React from "react";
import Carousel from "react-bootstrap/Carousel";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
const ProductCard = ({ product }) => {
  return (
    <>
      <Card style={{ width: "18rem" }}>
        <Carousel className="w-100">
          {product.images.map((image) => (
            <Carousel.Item interval={500}>
              <img className="d-block w-100" 
               onError={({currentTarget})=>{
                currentTarget.onerror = null; // prevent looping
                currentTarget.src= "https://developers.elementor.com/docs/assets/img/elementor-placeholder-image.png"
               }}
              src={image?image:"https://developers.elementor.com/docs/assets/img/elementor-placeholder-image.png"} alt="Second slide" />
            </Carousel.Item>
          ))}
        </Carousel>
        <Card.Body>
          <div className="d-flex justify-content-between align-items-center">
            <Card.Title>{product.title}</Card.Title>
            <strong className="text-warning h3">{product.price}$</strong>
          </div>

          <p>
            Category: <strong> {product.category.name}</strong>{" "}
          </p>

          <Card.Text>{product.description}</Card.Text>
          <Button variant="primary">Go somewhere</Button>
        </Card.Body>
      </Card>
    </>
  );
};

export default ProductCard;
