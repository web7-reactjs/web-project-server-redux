import logo from "./logo.svg";
import "./App.css";
import AllUsers from "./pages/AllUsers";
import "bootstrap/dist/css/bootstrap.min.css";
import MyNavBar from "./components/MyNavBar";
import { Routes, Route, Link } from "react-router-dom";
import ViewProfile from "./pages/ViewProfile";
import AllProduct from "./pages/AllProduct";
import InsertProduct from "./pages/InsertProduct";
import NotFoundPage from "./pages/NotFoundPage";
import InsertUser from "./pages/InsertUser";
function App() {
  return (
    <div>
      <MyNavBar />
      <Routes>

        <Route  index element={<AllProduct/>}/>
        <Route  path="/products" element={<AllProduct/>}/>
        <Route path="/newproduct" element={<InsertProduct/>}/>
        <Route  path="/users" element={<AllUsers/>}/>
        <Route  path="/newuser" element={<InsertUser/>}/>
        <Route path="/users/:id" element={<ViewProfile/>}/>
        <Route path="*" element={<NotFoundPage/>}/>
      </Routes>

      
    </div>
  );
}

export default App;
